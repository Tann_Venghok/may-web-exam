import React from "react";
import "./App.css";
import BookList from "../src/component/BookList";
import { Table } from "react-bootstrap";

function App() {
  constructor( 
    this.state = {
      books: [
        {
          id: 1,
          title: "មនុស្សពីរនាក់នៅផ្ទះជិតគ្នា",
          publishedYear: 2012,
          isHiding: false,
        },
        {
          id: 2,
          title: "គង់ហ៊ាន",
          publishedYear: 2015,
          isHiding: false,
        },
        {
          id: 3,
          title: "បុរសចេះថ្នាំពិសពស់",
          publishedYear: 2018,
          isHiding: false,
        },
        {
          id: 4,
          title: "អណ្ដើកនិងស្វា",
          publishedYear: 2019,
          isHiding: false,
        },
      ],
    }
  }
}
  

  return (
    <div className="App">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
      </Table>
    </div>
  );
}

export default App;
